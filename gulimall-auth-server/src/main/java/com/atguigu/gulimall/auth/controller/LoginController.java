package com.atguigu.gulimall.auth.controller;


import com.alibaba.fastjson.TypeReference;
import com.atguigu.common.constant.AuthServerConstant;
import com.atguigu.common.exception.BizCodeEnume;
import com.atguigu.common.utils.R;
import com.atguigu.common.vo.MemberResponseVo;
import com.atguigu.gulimall.auth.feign.MemberFeignService;
import com.atguigu.gulimall.auth.feign.ThirdPartFeignService;
import com.atguigu.gulimall.auth.vo.UserLoginVo;
import com.atguigu.gulimall.auth.vo.UserRegistVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;


@Controller
public class LoginController {


    /*
    发送一个请求直接跳转到页面，springMvc将页面请求映射过来

    * */

// 在GulimallWebConfig设置跳转逻辑
//    @GetMapping("/login.html")
//    public String loginPage(){
//
//        return "login";
//    }
//
//    @GetMapping("/reg.html")
//    public String regPage(){
//
//        return "reg";
//    }
    @Autowired
    ThirdPartFeignService thirdPartFeignService;

    @Autowired
    StringRedisTemplate redisTemplate;

    @Autowired
    MemberFeignService memberFeignService;

    @GetMapping("/sms/sendcode")
    public R sendCode(@RequestParam("phone") String phone){
        // TODO 1. 接口防止恶意攻击无限刷验证码
        String redisCode = redisTemplate.opsForValue().get(AuthServerConstant.SMS_CODE_CACHE_PREFIX + phone);
        if(!StringUtils.isEmpty(redisCode)){
            long currentTime = Long.parseLong(redisCode.split("_")[1]);
            //  60s内不能再发，防止同一个手机号在60秒内再次发送验证码
            if(System.currentTimeMillis() - currentTime  < 60000){
                return R.error(BizCodeEnume.SMS_CODE_EXCEPTION.getCode(), BizCodeEnume.SMS_CODE_EXCEPTION.getMessage());
            }
        }
        // 2.验证码的再次校验，redis存储非永久数据,存key-phone，calue-code  sms:code:13215902912->45614
        //2、验证码的再次效验 redis.存key-phone,value-code
        int code = (int) ((Math.random() * 9 + 1) * 100000);
        String codeNum = String.valueOf(code);
        String redisStorage = codeNum + "_" + System.currentTimeMillis();

        //存入redis,且设置20分钟有效期，刷新页面的时候继续可以用验证码
        redisTemplate.opsForValue().set(AuthServerConstant.SMS_CODE_CACHE_PREFIX+phone,
                redisStorage,10, TimeUnit.MINUTES);

        thirdPartFeignService.sendCode(phone, codeNum);

        return R.ok();

    }

    //TODO 重定向携带数据，利用session原理，将数据放在session中，只要跳到下一个页面取出数据后，session里面的数据就会被删除
    // 重定向携带数据
    // 需要解决分布式下session问题

    @PostMapping("/regist")
    // @Valid 开启校验   BindingResult返回校验结果, RedirectAttributes就是重定向用的
    public String regist(@Valid UserRegistVo vo, BindingResult result,
                         RedirectAttributes attributes){

        // 前置校验， @Valid 开启校验   BindingResult返回校验结果
        if(result.hasErrors()){
            Map<String, String> errors = result.getFieldErrors().stream().collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));
            attributes.addFlashAttribute("errors",errors);

            return "redirect:http://auth.gulimall.com/reg.html";
        }

        //  真正注册，调用远程服务进行注册
        // 1、校验验证码
        String code = vo.getCode();

        // 注册成功后，回到登录页面

        // 拿到验证码数据
        String s = redisTemplate.opsForValue().get(AuthServerConstant.SMS_CODE_CACHE_PREFIX + vo.getPhone());
        if(!StringUtils.isEmpty(s)){
            if(code.equals(s.split("_")[0])){
                //验证码校验成功后马上删除验证码; 令牌机制
                redisTemplate.delete(AuthServerConstant.SMS_CODE_CACHE_PREFIX + vo.getPhone());

                //验证码通过,真正调用远程服务进行注册
                R r = memberFeignService.regist(vo);
                if(r.getCode() == 0){
                    //成功
                    return "redirect:http://auth.gulimall.com/login.html";

                }else {
                    HashMap<String, String> errors= new HashMap<>();
                    errors.put("msg",r.getData("msg",new TypeReference<String>(){}));
                    attributes.addFlashAttribute("errors",errors);
                    return "redirect:http://auth.gulimall.com/reg.html";
                }


            }else{
                Map<String, String> errors = new HashMap<>();
                errors.put("code","验证码错误");
                attributes.addFlashAttribute("errors",errors);
                return "redirect:http://auth.gulimall.com/reg.html";
            }

        }else{
            Map<String, String> errors = new HashMap<>();
            errors.put("code","验证码错误");
            attributes.addFlashAttribute("errors",errors);
            return "redirect:http://auth.gulimall.com/reg.html";
        }

    }
//
//
//    @GetMapping("/login.html")
//    public String loginPage(HttpSession session){
//
//        Object attribute = session.getAttribute(AuthServerConstant.LOGIN_USER);
//        if(attribute == null){
//            // 没登录
//            return "login";
//        }else{
//            return "redirect:http://gulimall.com";
//        }
//
//    }
//
    @PostMapping("/login")
    // 页面提交的不是json，而是key-value不能写@RequestBody注解
    public String login(UserLoginVo vo, RedirectAttributes redirectAttributes,
                        HttpSession session){

        //远程登录

        R r = memberFeignService.login(vo);
        if(r.getCode() == 0){
            //成功
            MemberResponseVo data = r.getData("data", new TypeReference<MemberResponseVo>(){});
            session.setAttribute(AuthServerConstant.LOGIN_USER,data);
            return "redirect:http://gulimall.com";
        }else{
            HashMap<String, String> errors = new HashMap<>();
            errors.put("msg", r.getData("msg", new TypeReference<String>() {
            }));
            redirectAttributes.addFlashAttribute("errors",errors);
            return "redirect:http://auth.gulimall.com/login.html";
        }


    }

}
