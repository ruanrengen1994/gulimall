package com.atguigu.gulimall.auth.feign;

import com.atguigu.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@FeignClient("gulimall-third-party")

public interface ThirdPartFeignService {

    @ResponseBody //返回json数据
    @GetMapping("/sms/sendcode")
    public R sendCode(@RequestParam("phone") String phone,
                      @RequestParam("code") String code);

}
