package com.atguigu.gulimall.member.exception;

// 运行时异常也能抛出去
public class UsernameExistException extends RuntimeException {

    public UsernameExistException() {
        super("用户名已存在");
    }
}
