package com.atguigu.gulimall.member.vo;

import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class MemberRegistVo {

    private String userName;

    private String password;

    private String phone;
}
