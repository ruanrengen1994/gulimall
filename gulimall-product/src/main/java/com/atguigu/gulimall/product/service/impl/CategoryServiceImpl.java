package com.atguigu.gulimall.product.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.atguigu.gulimall.product.service.CategoryBrandRelationService;
import com.atguigu.gulimall.product.vo.Catelog2Vo;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;

import com.atguigu.gulimall.product.app.CategoryDao;
import com.atguigu.gulimall.product.entity.CategoryEntity;
import com.atguigu.gulimall.product.service.CategoryService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;



@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {

//    @Autowired
//    CategoryDao categoryDao;

    @Autowired
    CategoryBrandRelationService categoryBrandRelationService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    RedissonClient redisson;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(
                new Query<CategoryEntity>().getPage(params),
                new QueryWrapper<CategoryEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<CategoryEntity> listWithTree() {
        //1、查出所有分类
        List<CategoryEntity> entities = baseMapper.selectList(null);

        //2、组装成父子的树形结构

        //2.1）、找到所有的一级分类
        List<CategoryEntity> level1Menus = entities.stream().filter(categoryEntity ->
                categoryEntity.getParentCid() == 0
        ).map((menu)->{
            menu.setChildren(getChildrens(menu,entities));
            return menu;
        }).sorted((menu1,menu2)->{
            return (menu1.getSort()==null?0:menu1.getSort()) - (menu2.getSort()==null?0:menu2.getSort());
        }).collect(Collectors.toList());


        return level1Menus;
    }

    @Override
    public void removeMenuByIds(List<Long> asList) {
        //TODO  1、检查当前删除的菜单，是否被别的地方引用

        //逻辑删除
        baseMapper.deleteBatchIds(asList);
    }

    //[2,25,225]
    @Override
    public Long[] findCatelogPath(Long catelogId) {
        List<Long> paths = new ArrayList<>();
        List<Long> parentPath = findParentPath(catelogId, paths);

        Collections.reverse(parentPath);


        return parentPath.toArray(new Long[parentPath.size()]);
    }

    /**
     * 级联更新所有关联的数据
     * @param category
     */
//    @Caching(evict={
//            @CacheEvict(value = {"category"},key = "'level1Categorys'"),
//            @CacheEvict(value = {"category"},key = "'getCatelogJson'")
//    })
    @CacheEvict(value = {"category"},allEntries = true) // 写数据的失效模式
    @Transactional
    @Override
    public void updateDetail(CategoryEntity category) {
        this.updateById(category);
        categoryBrandRelationService.updateCategory(category.getCatId(),category.getName());
    }

    @Cacheable(value = {"category"},key = "#root.method.name")//有缓存则不调用方法，没有才调用
    @Override
    public List<CategoryEntity> getLevel1Categorys() {
        System.out.println("getLevel1Categorys...");
        long l = System.currentTimeMillis();

        List<CategoryEntity> categoryEntities = baseMapper.selectList(new QueryWrapper<CategoryEntity>().eq("parent_cid", 0));
        return categoryEntities;
    }

    // TODO 内存不够，产生堆外内存溢出OutofDerectMemoryError
    // 1)、SpringBoot2.0以后默认使用lettuce作为操作redis的客户端，它使用netty进行网络通信
    // 2)、lettuce的bug导致netty外内存溢出，-Xmx300m；netty如果没有指定堆外内存，默认使用-Xmx300m
    // 可以通过-Dio.netty.maxDirectMemory进行设置
    //解决方案：
      /*
       1）、设计lettuce客户端  X
       2）、切换使用jedis  : 被采用
       3）、

       */

    @Cacheable(value = {"category"},key = "#root.methodName")
    @Override
    public Map<String, List<Catelog2Vo>> getCatalogJson() {
        //一次性查询出所有的分类数据，减少对于数据库的访问次数，后面的数据操作并不是到数据库中查询，而是直接从这个集合中获取，
        // 由于分类信息的数据量并不大，所以这种方式是可行的
        System.out.println("查询数据库。。。。。");
        /*1、将数据库查询由多次变为一次
         * */
        List<CategoryEntity> selectList = baseMapper.selectList(null);
        // 1.查出所有一级分类
        List<CategoryEntity> level1Categorys = getParent_cid(selectList, 0L);
        // 2.封装数据
        Map<String, List<Catelog2Vo>> parent_cid = level1Categorys.stream().collect(Collectors.toMap(k -> k.getCatId().toString(), v -> {
            // 遍历你每一个一级分类，查到这个一级分类的所有二级分类
            List<CategoryEntity> categoryEntities = getParent_cid(selectList, v.getCatId());
            List<Catelog2Vo> catelog2Vos = null;
            if (categoryEntities != null) {
                catelog2Vos = categoryEntities.stream().map(l2 -> {
                    Catelog2Vo catelog2Vo = new Catelog2Vo(v.getCatId().toString(), null, l2.getCatId().toString(), l2.getName());
                    // 1.当前二级分类的三级分类封装成vo
                    List<CategoryEntity> level3Catelog = getParent_cid(selectList, l2.getCatId());
                    if (level3Catelog != null) {
                        List<Catelog2Vo.Catelog3Vo> collect = level3Catelog.stream().map(l3 -> {
                            // 2、封装成指定格式
                            Catelog2Vo.Catelog3Vo catelog3Vo = new Catelog2Vo.Catelog3Vo(l2.getCatId().toString(), l3.getCatId().toString(), l3.getName());
                            return catelog3Vo;
                        }).collect(Collectors.toList());

                        catelog2Vo.setCatalog3List(collect);
                    }
                    return catelog2Vo;
                }).collect(Collectors.toList());
            }


            return catelog2Vos;
        }));

        return parent_cid;
    }


    @Deprecated
    public Map<String, List<Catelog2Vo>> getCatalogJsons() {


        /*
         * 1、空结果缓存： 解决缓存穿透
         * 2、设置过期时间（加随机值）:解决缓存雪崩
         * 3、加锁：解决缓存击穿
         * */



        //给缓存中放json字符串拿出的json字符串，还用逆转为能用的对象类型：【序列化与反序列化】
        // 1、加入缓存逻辑，缓存中存的数据是json字符串
        // JSON是跨语言，跨平台兼容
        String catalogJSON = stringRedisTemplate.opsForValue().get("catalogJSON");
        if (StringUtils.isEmpty(catalogJSON)) {
            // 2、缓存中没有数据，查询数据库
            System.out.println("缓存不命中。。需要去数据库查询，稍等。。。");
            Map<String, List<Catelog2Vo>> catalogJsonFromDb = getCatalogJsonFromDbWithRedissonLock();

            return catalogJsonFromDb;
        }
        // 转为指定的对象,JSON作为数据传输的流，然后发送接收端都得处理这个JSON流为自己需要的类型对象

        System.out.println("缓存命中。。直接返回。。。");

        Map<String, List<Catelog2Vo>> result = JSON.parseObject(catalogJSON,new TypeReference<Map<String, List<Catelog2Vo>> >(){});

        return result;

    }


    /*
     * 缓存里面的数据如何和数据库保持一致
     * 数据的一致性
     *   1)、双写模式
     *   2）、失效模式
     * */
    public Map<String, List<Catelog2Vo>> getCatalogJsonFromDbWithRedissonLock() {

        // 1、占分布式锁，去redis占坑
        // 注意锁的名字,锁的粒度越细越快
        // 锁的粒度：具体缓存的是某个数据，11号商品：product-11-lock，product-12-lock，名字如果都设置为prouct-lock
        // 的话，万一10个请求11,100万请请求12，则万一锁先被12拿到，则11号就得等100万12号搞定后，很不科学
        RLock lock = redisson.getLock("CatalogJson-lock");
        lock.lock();//锁住后面的代码

        Map<String, List<Catelog2Vo>> dataFromDb;
        try{
            dataFromDb = getDataFromDb();
        }finally {
            lock.unlock();
        }

        return dataFromDb;


    }


    // 从数据库查询并封装分类数据,redis分布式锁的方式
    public Map<String, List<Catelog2Vo>> getCatalogJsonFromDbWithRedisLock() {

        // 1、占分布式锁，去redis占坑, 占到坑的时候需要设置过期时间，设置过期时间必须和加锁是同步的，原子的
        // 为了防止自己拿到的锁被别的线程删除，"lock"的值不随便取值，而是设为UUID
        String uuid = UUID.randomUUID().toString();
        Boolean lock = stringRedisTemplate.opsForValue().setIfAbsent("lock", uuid,300,TimeUnit.SECONDS); //300s才过期，保证业务getDataFromDb()执行完时锁还在
        Map<String, List<Catelog2Vo>> dataFromDb;
        if(lock){
            System.out.println("获取分布式锁成功。。。。");
            // 加锁成功...执行业务
            try{
                dataFromDb = getDataFromDb();
            }finally {
                String script = "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end";
                //  删除锁也是原子逻辑
                Long lock1 = stringRedisTemplate.execute(new DefaultRedisScript<Long>(script,Long.class)
                        , Arrays.asList("lock" ), uuid);
            }

            return dataFromDb;
//            stringRedisTemplate.delete("lock");//直接删除锁,有可能删除别人的锁
            // 获取值对比+对比成功删除=原子操作 lua脚本解锁
            //String lockValue = stringRedisTemplate.opsForValue().get("lock");
           /*
                if(uuid.equals(lockValue)){
                // 删除我自己的锁
                stringRedisTemplate.delete("lock");
            }*/

        }else {
            // 枷锁失败。。。synchronized()，等待1000ms重试
            // 休眠100ms重试
            System.out.println("获取分布式锁失败。。等待重试");
            try{
                Thread.sleep(200);
            }catch (Exception e){

            }

            return getCatalogJsonFromDbWithRedisLock(); // 自旋的方式

        }


    }


    private Map<String, List<Catelog2Vo>> getDataFromDb() {
        String catalogJSON = stringRedisTemplate.opsForValue().get("catalogJSON");
        if (!StringUtils.isEmpty(catalogJSON)) {

            // 缓存不为空直接返回
            Map<String, List<Catelog2Vo>> result = JSON.parseObject(catalogJSON, new TypeReference<Map<String, List<Catelog2Vo>>>() {
            });

            return result;
        }

        System.out.println("查询数据库。。。。。");
        /*1、将数据库查询由多次变为一次
         * */
        List<CategoryEntity> selectList = baseMapper.selectList(null);
        // 1.查出所有一级分类
        List<CategoryEntity> level1Categorys = getParent_cid(selectList, 0L);
        // 2.封装数据
        Map<String, List<Catelog2Vo>> parent_cid = level1Categorys.stream().collect(Collectors.toMap(k -> k.getCatId().toString(), v -> {
            // 遍历你每一个一级分类，查到这个一级分类的所有二级分类
            List<CategoryEntity> categoryEntities = getParent_cid(selectList, v.getCatId());
            List<Catelog2Vo> catelog2Vos = null;
            if (categoryEntities != null) {
                catelog2Vos = categoryEntities.stream().map(l2 -> {
                    Catelog2Vo catelog2Vo = new Catelog2Vo(v.getCatId().toString(), null, l2.getCatId().toString(), l2.getName());
                    // 1.当前二级分类的三级分类封装成vo
                    List<CategoryEntity> level3Catelog = getParent_cid(selectList, l2.getCatId());
                    if (level3Catelog != null) {
                        List<Catelog2Vo.Catelog3Vo> collect = level3Catelog.stream().map(l3 -> {
                            // 2、封装成指定格式
                            Catelog2Vo.Catelog3Vo catelog3Vo = new Catelog2Vo.Catelog3Vo(l2.getCatId().toString(), l3.getCatId().toString(), l3.getName());
                            return catelog3Vo;
                        }).collect(Collectors.toList());

                        catelog2Vo.setCatalog3List(collect);
                    }
                    return catelog2Vo;
                }).collect(Collectors.toList());
            }


            return catelog2Vos;
        }));

        // 3、查到的数据 再放入缓存,将对象转为json放在缓存中
        String s = JSON.toJSONString(parent_cid);
        // stringRedisTemplate.opsForValue().set("catalogJSON",s,1,TimeUnit.DAYS);
        stringRedisTemplate.opsForValue().set("catalogJSON", s, 1, TimeUnit.DAYS);

        return parent_cid;
    }

    // 从数据库查询并封装分类数据,本地锁的方式
    public Map<String, List<Catelog2Vo>> getCatalogJsonFromDbWithLocalLock() {

        // 只要是同一把锁，就能锁住这个锁的所有线程
        // 1、 synchronized(this):所有的组件在容器中都是单例的(部署在一台服务器一个tomcat里面）
        // TODO 本地锁synchronized，JUC(Lock)，在分布式情况下，想要锁住所有，必须使用分布式锁
        synchronized (this){
            // 得到锁以后，我们应该在缓存中再确定一次缓存，缓存没有再去查数据库
            return getDataFromDb();
        }

    }

    private List<CategoryEntity> getParent_cid(List<CategoryEntity> selectList,Long parent_cid) {
        //return baseMapper.selectList(new QueryWrapper<CategoryEntity>().eq("parent_cid", v.getCatId()));
        List<CategoryEntity> collect = selectList.stream().filter(item -> item.getParentCid() == parent_cid).collect(Collectors.toList());
        return collect;
    }


    //225,25,2
    private List<Long> findParentPath(Long catelogId,List<Long> paths){
        //1、收集当前节点id
        paths.add(catelogId);
        CategoryEntity byId = this.getById(catelogId);
        if(byId.getParentCid()!=0){
            findParentPath(byId.getParentCid(),paths);
        }
        return paths;

    }


    //递归查找所有菜单的子菜单
    private List<CategoryEntity> getChildrens(CategoryEntity root,List<CategoryEntity> all){

        List<CategoryEntity> children = all.stream().filter(categoryEntity -> {
            return categoryEntity.getParentCid() == root.getCatId();
        }).map(categoryEntity -> {
            //1、找到子菜单
            categoryEntity.setChildren(getChildrens(categoryEntity,all));
            return categoryEntity;
        }).sorted((menu1,menu2)->{
            //2、菜单的排序
            return (menu1.getSort()==null?0:menu1.getSort()) - (menu2.getSort()==null?0:menu2.getSort());
        }).collect(Collectors.toList());

        return children;
    }



}