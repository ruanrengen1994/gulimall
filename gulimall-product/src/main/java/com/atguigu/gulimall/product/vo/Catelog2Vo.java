package com.atguigu.gulimall.product.vo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

// 二级分类vo
// 有参无参构造器
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Catelog2Vo {


    private String catalog1Id;     // 一级父分类
    private List<Catelog3Vo> catalog3List;    // 三级子分类
    private String id;
    private String name;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    // 3级分类vo
    public static class Catelog3Vo {

        private String catalog2Id;
        private String id;
        private String name;

    }


}
