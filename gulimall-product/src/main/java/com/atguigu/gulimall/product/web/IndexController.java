package com.atguigu.gulimall.product.web;


import com.atguigu.gulimall.product.entity.CategoryEntity;
import com.atguigu.gulimall.product.service.CategoryService;
import com.atguigu.gulimall.product.vo.Catelog2Vo;
import org.redisson.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@Controller
public class IndexController {
/*    * 在ResourceProperties.class，
  new String[]{"classpath:/META-INF/resources/", "classpath:/resources/", "classpath:/static/", "classpath:/public/"};
**/

    @Autowired
    CategoryService categoryService;

    @Autowired
    RedissonClient redisson;

    @Autowired
    StringRedisTemplate redisTemplate;


    @GetMapping({"/","index.html"})
    public String indexPage(Model model){

        // TODO 1、查出所有的一级分类
       List<CategoryEntity> categoryEntities = categoryService.getLevel1Categorys();
        // 视图解析器进行拼串
        // "classpath:/templates/返回值.html

        model.addAttribute("categorys",categoryEntities);
        return "index";
    }

    // index/catalog.json
    @ResponseBody
    @GetMapping("index/catalog.json")
    public Map<String, List<Catelog2Vo>> getCatalogJson(){

        Map<String, List<Catelog2Vo>> catalogJson = categoryService.getCatalogJson();
        return catalogJson;
    }


    @GetMapping("/hello")
    @ResponseBody
    public String hello(){
        //1.获取一把锁，只要名字一样，就是同一把锁
        RLock lock = redisson.getLock("my-lock");
        //2.加锁和解锁

        try {
            lock.lock(); // 阻塞式等待
            // 1）、锁的自动续期，如果业务超长，比30S还长，运行期间自动续期
            // 不必担心业务时间长，锁被自动删除掉

            // 2)、加锁的业务只要运行完成，就不会给当前锁续期，即使不主动解锁，锁默认在30s释放
            System.out.println("加锁成功，执行业务方法..."+ Thread.currentThread().getId());
            Thread.sleep(30000);
        } catch (Exception e){

        }finally {
            lock.unlock();
            System.out.println("读锁释放..."+ Thread.currentThread().getId());
        }
        return "hello";
    }


    /*
    * 保证一定能读到最新数据，修改期间，写锁是一个互斥锁（排他锁，独享锁），读锁是一个共享锁
    * 写锁没释放读就必须等待
    * 读+读：相当于无锁，并发读，只会在redis中记录好，所有当前的读锁，他们都会同时加锁成功
    * 写+读：等待写释放了才能读
    * 写+写：阻塞方式
    * 读+写：有读锁，写也需要等待
    *
    * 只要有写的存在，都必须等待
    * */
    @GetMapping("/write")
    @ResponseBody
    public String writeValue(){
        RReadWriteLock writeLock= redisson.getReadWriteLock("rw-loc");
        String uuid = null;
        RLock rlock = writeLock.writeLock();
        rlock.lock();
        try {
            System.out.println("写锁加锁成功.."+ Thread.currentThread().getId());
            uuid = UUID.randomUUID().toString();
            redisTemplate.opsForValue().set("writeValue",uuid);
            Thread.sleep(30000);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            rlock.unlock();
            System.out.println("写锁释放.."+ Thread.currentThread().getId());
        }
        return uuid;
    }

    @GetMapping("/read")
    @ResponseBody
    public String readValue(){
        String uuid = null;
        RReadWriteLock readLock=redisson.getReadWriteLock("rw-loc");
        RLock rlock = readLock.readLock();
        rlock.lock();
        try {
            System.out.println("读锁加锁成功.."+ Thread.currentThread().getId());
            uuid = redisTemplate.opsForValue().get("writeValue");
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            rlock.unlock();
            System.out.println("读锁释放.."+ Thread.currentThread().getId());
        }
        return uuid;
    }


    @GetMapping("/lockDoor")
    @ResponseBody
    public String lockDoor() throws InterruptedException {
        RCountDownLatch door = redisson.getCountDownLatch("door");
        door.trySetCount(5);
        door.await();  //等待闭锁都完成

        return  "放假了";
    }

    @GetMapping("/gogogo/{id}")
    @ResponseBody
    public String gogogo(@PathVariable("id") Long id) throws InterruptedException {
        RCountDownLatch door = redisson.getCountDownLatch("door");
        door.countDown();

        return  id+"班的人都走了";
    }

    @GetMapping("/park")
    @ResponseBody
    public String park() throws InterruptedException {
        RSemaphore park = redisson.getSemaphore("park");
        boolean b = park.tryAcquire();
        if(b){
            // 执行业务
        }else {
            return "error";
        }        //获取一个信号，获取一个值，站一个车位

        return "ok=>"+b;
    }

    @GetMapping("/go")
    @ResponseBody
    public String go() throws InterruptedException {
        RSemaphore park = redisson.getSemaphore("park");
        park.release();//释放一个车位

        return "ok";
    }




}
