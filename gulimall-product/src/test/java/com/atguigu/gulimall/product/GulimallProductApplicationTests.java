package com.atguigu.gulimall.product;

import com.atguigu.gulimall.product.entity.BrandEntity;
import com.atguigu.gulimall.product.service.BrandService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.junit.jupiter.api.Test;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@SpringBootTest
class GulimallProductApplicationTests {

    @Autowired
    RedissonClient redissonClient;

    @Autowired
    BrandService brandService;

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @Test
    void contextLoads() {
    }

    @Test
    public void testBrandsql(){

        BrandEntity brandEntity = new BrandEntity();

//        brandEntity.setName("特斯拉");
//
//        brandEntity.setDescript("特斯拉");
//
//        brandService.save(brandEntity);
//        System.out.println("保存成功");

        // 查询：

        List<BrandEntity> list = brandService.list(new QueryWrapper<BrandEntity>().eq("name", "华为"));

        list.forEach((item)->{
            System.out.println(item);
        });


    }

    @Test
    public void sorted(){

        int[] myArray = {3,38,5,4,44,48,50,19,26,14,2,27,42};
        int len = myArray.length;
        /**
         * 冒泡排序
         */

/*        for(int i = 0;i < len;i++){
            for(int j = 0;j < len-i-1;j++){
                if(myArray[j]>myArray[j+1]){
                    int temp = myArray[j+1];
                    myArray[j+1] = myArray[j];
                    myArray[j] = temp;
                }
            }
        }*/

        /**
         * 选择排序
         */
/*
        for (int i=0; i < len; i++){

            // 将当前位置设为最小值
            int min = i;

            // 检查数组其余部分是否更小
            for (int j=i+1; j < len; j++){
                if (myArray[j] < myArray[min]){
                    min = j;
                }
            }

            // 如果当前位置不是最小值，将其换为最小值
            if (i != min){
                var temp = myArray[i];
                myArray[i] = myArray[min];
                myArray[min] = temp;
            }
        }
*/

        /**
         * 插入排序
         */


            // 插入排序


            for (int i = 1; i < len; i++) {
                int value = myArray[i];
                int j = i - 1;
                // 查找插入的位置
                for (; j >= 0; j--) {
                    if (myArray[j] > value) {
                        myArray[j+1] = myArray[j];  // 数据移动
                    } else {
                        break;
                    }
                }
                myArray[j+1] = value; // 插入数据
            }


        /**
         *  归并排序
         */


        System.out.println(len);
        System.out.println(Arrays.toString(myArray));
        System.out.println(myArray.length);


    }








    @Test
    public void teststringRedisTemplate(){

        ValueOperations<String, String> ops = stringRedisTemplate.opsForValue();

        // 保存
        ops.set("hello","world"+ UUID.randomUUID().toString());

        //查询

        String hello = ops.get("hello");
        System.out.println("之前保存的数据是:"+hello);

    }



    @Test
    public void testRedison(){
        System.out.println(redissonClient);
    }
}
