package com.atguigu.gulimall.search.controller;


import com.atguigu.gulimall.search.service.MallSearchService;
import com.atguigu.gulimall.search.vo.SearchParam;
import com.atguigu.gulimall.search.vo.SearchResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.ui.Model;
import javax.servlet.http.HttpServletRequest;

@Controller
public class SearchController {
//
    @Autowired
    private MallSearchService mallSearchService;

    @GetMapping({"/","/list.html"})
    public String ListPage(SearchParam param, Model model, HttpServletRequest request){

        param.set_queryString(request.getQueryString());

        //1. 根据页面传递过来的查询参数，到ES中检索商品
        SearchResult result= mallSearchService.search(param);
        model.addAttribute("result",result);

        return "list";
    }
}
