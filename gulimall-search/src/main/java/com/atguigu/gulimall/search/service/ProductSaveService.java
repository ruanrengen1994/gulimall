package com.atguigu.gulimall.search.service;

import com.atguigu.common.to.es.SkuEsModel;
import org.apache.ibatis.annotations.Param;

import java.io.IOException;
import java.util.List;


public interface ProductSaveService {


    public boolean productStatusUp(@Param("skuEsModels") List<SkuEsModel> skuEsModels) throws IOException;
}
