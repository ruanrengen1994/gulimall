package com.atguigu.gulimall.search.thread;


import java.util.Arrays;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadTest {

    public static ExecutorService executorService = Executors.newFixedThreadPool(10);

    public static void main(String[] args) {

        /*
        * Java中创建线程主要有三种方式：

    * */
        /*1.继承Thread类
         System.out.println("main......start.....");
         Thread thread = new Thread01();
         thread.start();
         System.out.println("main......end.....");
         */

         /*2.实现Runnable接口
         Runable01 runable01 = new Runable01();
         new Thread(runable01).start();
         */

        /*3.使用Callable和FutureTask
         FutureTask<Integer> futureTask = new FutureTask<>(new Callable01());
         new Thread(futureTask).start();

         //阻塞進程
         System.out.println(futureTask.get());
         */

//        4.使用线程池
                

         /*
        service.execute(new Runable01());
         Future<Integer> submit = service.submit(new Callable01());
         submit.get();
         */
         
         // 本项目以后所有的任务都交给线程执行
        // 1.方式1：new Thread(()-> System.out.println("hello")).start();

        //当前系统中的池只有一两个，每个异步任务，提交给线程池让他自己去执行

        //executorService.execute(new Runable01());
        //2.方式2

  /*      System.out.println("main....start");

        Thread01 thread01 = new Thread01();
        thread01.start();


        System.out.println("main....end");*/

        System.out.println("main....start");
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        CompletableFuture.runAsync(()->{
            System.out.println("当前线程： " + Thread.currentThread().getId());
            int i = 10 / 2;
            System.out.println("运行结果："+i);
        },executorService);

        System.out.println("main....end");

    }




    public static class Thread01 extends Thread {
        /*
        （1）继承Thread类并重写run方法
        （2）创建线程对象
        （3）调用该线程对象的start()方法来启动线程
        * */
        @Override
        public void run() {
            System.out.println("当前线程：" + Thread.currentThread().getId());
            int i = 10 / 2;
            System.out.println("运行结果：" + i);
        }
    }


    public static class Runable01 implements Runnable {
        @Override
        public void run() {
            System.out.println("当前线程：" + Thread.currentThread().getId());
            int i = 10 / 2;
            System.out.println("运行结果：" + i);
        }
    }


    public static class Callable01 implements Callable<Integer> {
        @Override
        public Integer call() throws Exception {
            System.out.println("当前线程：" + Thread.currentThread().getId());
            int i = 10 / 2;
            System.out.println("运行结果：" + i);
            return i;
        }
    }
}
