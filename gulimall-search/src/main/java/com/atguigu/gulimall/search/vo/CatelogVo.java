package com.atguigu.gulimall.search.vo;


import lombok.Data;

@Data
public class CatelogVo {
    private Long catelogId;
    private String catelogName;
}
