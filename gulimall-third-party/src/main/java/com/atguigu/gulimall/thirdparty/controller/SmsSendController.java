package com.atguigu.gulimall.thirdparty.controller;


import com.atguigu.common.utils.R;
import com.atguigu.gulimall.thirdparty.component.SmsComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController  // @ResponseBody ＋ @Controller合在一起的作用
@RequestMapping("/sms")
public class SmsSendController {
    // code是提供给别的服务进行调用的
    @Autowired
    SmsComponent smsComponent;

    @GetMapping(value = "/sendcode")
    public R sendCode(@RequestParam("phone") String phone, @RequestParam("code") String code){
        smsComponent.sendSmsCode(phone,code);
        return R.ok();
    }

}
